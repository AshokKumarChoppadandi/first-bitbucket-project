import sbt.ExclusionRule

name := "ProductsRecommendationSystem"

version := "1.0"

scalaVersion := "2.11.8"

resolvers += "Cloudera" at "https://repository.cloudera.com/content/repositories/releases/"

// https://mvnrepository.com/artifact/org.apache.spark/spark-core
libraryDependencies += "org.apache.spark" % "spark-core_2.11" % "2.2.2"

// https://mvnrepository.com/artifact/org.apache.spark/spark-launcher
//libraryDependencies += "org.apache.spark" % "spark-launcher_2.11" % "2.2.2"

// https://mvnrepository.com/artifact/org.apache.spark/spark-sql
libraryDependencies += "org.apache.spark" % "spark-sql_2.11" % "2.2.2" % "provided"

// https://mvnrepository.com/artifact/org.apache.spark/spark-mllib
libraryDependencies += "org.apache.spark" %% "spark-mllib" % "2.2.2" % "provided"

// https://mvnrepository.com/artifact/org.apache.hive/hive-exec
libraryDependencies += "org.apache.spark" % "spark-hive_2.11" % "2.2.0" % "provided"

// https://mvnrepository.com/artifact/org.apache.hive/hive-jdbc
//libraryDependencies += "org.apache.hive" % "hive-jdbc" % "1.1.0-cdh5.12.1"

// https://mvnrepository.com/artifact/com.google.guava/guava
libraryDependencies += "com.google.guava" % "guava" % "16.0.1" % "provided"

libraryDependencies += "org.apache.hive" % "hive-jdbc" % "1.1.0-cdh5.12.1"

libraryDependencies += "com.typesafe" % "config" % "1.2.1"

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}