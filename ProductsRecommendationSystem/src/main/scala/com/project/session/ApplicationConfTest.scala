package com.project.session

/**
  * Created by chas6003 on 30-11-2018.
  */

import com.typesafe.config.{ConfigFactory, ConfigObject}

object ApplicationConfTest {
  def main(args: Array[String]): Unit = {

    val configFile = if(args.length == 1) {
      args(0).trim
    } else {
      "application.conf"
    }
    println("Input Config File :: " + configFile)
    val config = ConfigFactory.load(configFile)
    /*
    val tab1 = config.getStringList("prs.tables.sourceTable")
    val tab2 = config.getString("prs.tables.targetTable")

    println("Tables----------> ")
    val tmpList = tab1.toArray.toList
    tmpList.foreach(println(_))
    println("Table 2 ----------> " + tab2)
    */

    //val incrementalTables = config.getObjectList("prs.tables.incrementTables")
    val incrementalTables = config.getObjectList("prs.tables.incrementTables")
    val len = incrementalTables.size()
    for (x <- 0 until len) {
      val sourceTable = incrementalTables.get(x).get("sourceTable").render().replace("\"", "")
      val targetTable = incrementalTables.get(x).get("targetTable").render().replace("\"", "")
      println("SourceTable :: "  + sourceTable + ", TargetTable :: " + targetTable)
    }

    val jdbcSourceUrl = config.getString("prs.jdbcUrls.sourceUrl")
    val jdbcTargetUrl = config.getString("prs.jdbcUrls.targetUrl")

    println("jdbcSourceUrl :: " + jdbcSourceUrl)
    println("jdbcTargetUrl :: " + jdbcTargetUrl)

    /*
    val tableLength = incrementalTables
    val oneTable = incrementalTables.get(0)
    val sourceTable = oneTable.get("sourceTable").render().replace("\"", "")
    println(s"Soruce Table :: ${sourceTable}")
    */
  }
}
