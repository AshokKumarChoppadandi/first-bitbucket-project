package com.project.session

import java.sql.{Connection, DriverManager, ResultSet}

import scala.util.control.Breaks._

/**
  * Created by chas6003 on 19-11-2018.
  */

case class SessionRecord (userId : Int, searchQuery : String, timestamp : Long)

object UserSession {
  def main(args: Array[String]): Unit = {
    val driver = "org.apache.hive.jdbc.HiveDriver"
    val user = ""
    val password = ""

    val url = "jdbc:hive2://lhrrhegapd019.enterprisenet.org:10000/prs"

    var connection : Connection = null
    var sessionQueries : List[SessionRecord] = List()
    var userId : Int = 0
    try {
      Class.forName(driver)
      connection = DriverManager.getConnection(url,user,password)
      val statement = connection.createStatement()

      var resultSet : ResultSet = null

      println("\n********************* Welcome to www.XYZ.com *************************")
      var bool : Boolean = true
      while(bool) {
        breakable {
          print("Enter User ID :: ")
          try {
            userId = readLine.toInt
            bool = false
          } catch {
            case e : NumberFormatException => {
              println("Please Enter the User ID correctly...!!!")
              break
            }
          }
        }
      }

      var tmp : Boolean = true
      while(tmp) {
        print("\n1. Do you want to Search\n2. Enter the viewed products\n" + "\n0. Exit\nEnter Your Choice :: ")
        var choice : Any = null
        breakable {
          try {
            choice = readLine.toInt
            choice match {
              case 0 => println("Thanks for visiting the Page")
                val len = sessionQueries.length
                for(i <- 0 until len) {
                  val tmp : SessionRecord = sessionQueries(i)
                  val res = statement.executeUpdate("insert into table prs.user_sessions " + "values (" + tmp.userId + ",'" + tmp.searchQuery + "'," + tmp.timestamp + ")")
                  if(res < 0) {
                    println("Error in recording Users Session...!!!")
                  }
                }
                println(len + " Record(s) Updated Successfully")
                tmp = false

              case 1 => print("\nEnter the your Search Key Terms :: ")
                var in = readLine()
                in = in.trim.toLowerCase
                val ts = System.currentTimeMillis
                sessionQueries = sessionQueries :+ SessionRecord(userId, in, ts)
                val searchQuery =
                  s"""
                    | select movieid, title, genres from prs.movies_small
                    | where lower(title) like '%${in}%' or lower(genres) like '%${in}%'
                  """.stripMargin

                resultSet = statement.executeQuery(searchQuery)
                var i = 1
                println("The products you searched for")
                println("******************** Products **********************")
                while(resultSet != null && resultSet.next()) {
                  breakable {
                    println("\nProductID :: " + resultSet.getInt("movieid") +
                      "\nName :: " + resultSet.getString("title") +
                      "\nCategory :: " + resultSet.getString("genres"))
                    println()
                    i += 1
                    if(i > 10) {
                      resultSet = null
                    }
                  }
                }
                println("******************************************************")
              case 2 => var tmp1 = true
                while(tmp1) {
                  print("Please Enter the viewed Product ID :: ")
                  breakable {
                    try {
                      val product_id = readLine.toInt
                      val timestamp = System.currentTimeMillis
                      print("Want to rate the product (Enter y/N) ? :: ")
                      val rateIn = readChar
                      if(rateIn == 'y') {
                        print("Rate (1.0 to 5.0) to " + product_id + " product :: ")
                        try {
                          val rating = readLine.toFloat
                          val res = statement.executeUpdate("insert into table prs.user_products_ratings_small values (" +
                            userId + "," + product_id + "," + rating + "," + timestamp + ")")
                          if(res < 0) {
                            println("Error in inserting the Rating Record...!!!")
                          }
                        } catch {
                          case e : NumberFormatException => {
                            println("Product rating is not given correctly...!!!")
                          }
                          case _ : Throwable => println("Unknown Exception while rating the product...!!!")
                        }
                      } else {
                        val res = statement.executeUpdate("insert into table user_products_ratings values (" +
                          userId + "," + product_id + ", 0, " + timestamp + ")" )
                      }
                      print("Want to enter another Viewed product (Enter y/N)..??")
                      val input = readChar
                      if(input != 'y') {
                        println("Thank you for rating the Product")
                        tmp1 = false
                      }
                    } catch {
                      case e : NumberFormatException => {
                        println("Sorry...!!!\nPlease Enter the Correct Product ID..!!!")
                        break
                      }
                      case _ : Throwable => println("Error in inserting the Products details")
                    }
                  }
                }
              case _ : Throwable => println("Sorry..!!\nEnter Your Choice Correctly ..!!")
            }
          } catch {
            case e : NumberFormatException => {
              println("Sorry...!!!\n" + "Please Enter Your Choice Correctly ...!!!")
              break
            }
          }
        }
      }
    } catch {
      case e : Exception => e.printStackTrace
    }
    connection.close()
  }
}
