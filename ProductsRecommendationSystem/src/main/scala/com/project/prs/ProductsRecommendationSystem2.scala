package com.project.prs

import java.io.{BufferedWriter, OutputStreamWriter}
import java.net.URI

import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.mllib.recommendation.ALS
import org.apache.spark.mllib.recommendation.MatrixFactorizationModel
import org.apache.spark.mllib.recommendation.Rating
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._

/**
  * Created by chas6003 on 02-12-2018.
  */
object ProductsRecommendationSystem2 {
  var fs1: FileSystem = _

  def computeRmse(model: MatrixFactorizationModel, data: RDD[Rating], implicitPrefs: Boolean): Double = {
    val predictions: RDD[Rating] = model.predict(data.map(x => (x.user, x.product)))
    val predictionsAndRatings = predictions.map { x => ((x.user, x.product), x.rating) }
      .join(data.map(x => ((x.user, x.product), x.rating)))
      .values
    if (implicitPrefs) {
      println("(Prediction, Rating)")
      println(predictionsAndRatings.take(5).mkString("n"))
    }
    math.sqrt(predictionsAndRatings.map(x => (x._1 - x._2) * (x._1 - x._2)).mean())
  }

  def main(args: Array[String]): Unit = {
    //"args(0) = /user/chas6003/ProductsRecommendationSystem/MovieLensSmall/movies.csv"
    //"args(1) = /user/chas6003/ProductsRecommendationSystem/MovieLensSmall/ratings.csv"

    Logger.getLogger("org.apache.spark").setLevel(Level.WARN)
    Logger.getLogger("org.eclipse.jetty.server").setLevel(Level.OFF)

    val len : Int = args.length
    println("****************************************************************")
    println("Arguments ::: " + args.toList)
    println("Length ::: " + len)
    var i: Int = 0
    while (i < len) {
      println("Element :: " + i + " :: " + args(i))
      i = i + 1
    }
    println("****************************************************************")
    if (len < 2) {
      println("Usage: /path/to/spark/bin/spark-submit --driver-memory 1g --class ProductRecommendation " +
        "target/scala-*/productrecommendation.jar <USER ID> <NUMBER OF RECOMMENDATIONS>")
      sys.exit(1)
    }

    val spark = SparkSession.builder().appName("ProductsRecommendationSystem").enableHiveSupport().getOrCreate()

    val logFile = s"/user/chas6003/ProductsRecommendationSystem/Output/"
    fs1 = FileSystem.get(new URI(logFile), spark.sparkContext.hadoopConfiguration)

    var users : List[Int] = List()
    for (x <- 0 until len - 1) {
      val tmp = args(x).toInt
      users = users :+ tmp
    }

    val br_users = spark.sparkContext.broadcast(users)

    val number_of_recommendations = args(len-1).toInt

    //val productsDf = spark.read.option("header","true").csv(productsDataPath)
    val productsDf = spark.sql("select movieId as product_id, title as product_title, genres as product_category from prs.movies_small")
    //val ratingDf = spark.read.option("header","true").csv(ratingsDataPath)
    val ratingDf = spark.sql("select * from prs.user_ratings_small")

    //val products = productsDf.rdd.map(x => (x.getAs[Int](0), x.getAs[String](1))).collect().toMap
    //val products = productsDf.rdd.map(x => (x.getAs[Int](0), (x.getAs[String](1), x.getAs[String](2))))

    val ratings = ratingDf.rdd.map(x => (x.getAs[Long](3), Rating(x.getAs[Int](0), x.getAs[Int](1), x.getAs[Double](2)))).values
    val numRatings = ratings.count
    val numUsers = ratings.map(_.user).distinct.count
    val numProducts = ratings.map(_.product).distinct.count
    val numPartitions = 10

    println("Got " + numRatings + " ratings from " + numUsers + " users on " + numProducts + " products.")

    val Array(training, validation, test) = ratings.randomSplit(Array(0.6, 0.2, 0.2))
    val trainData = training.repartition(numPartitions).cache()
    val validationData = validation.repartition(numPartitions).cache()
    val testData = test.repartition(numPartitions).cache()

    val numTraining = trainData.count()
    val numValidation = validationData.count()
    val numTest = testData.count()

    println("Training: " + numTraining + ", validation: " + numValidation + ", test: " + numTest)

    val ranks = List(10, 20)
    val lambdas = List(0.1, 0.5)
    val numIters = List(15, 20)
    var bestModel: Option[MatrixFactorizationModel] = None
    var bestValidationRmse = Double.MaxValue
    var bestRank = 0
    var bestLambda = -1.0
    var bestNumIter = -1

    val alpha = 1.0
    val block = -1
    val seed = 12345L
    val implicitPrefs = false

    for (rank <- ranks; lambda <- lambdas; numIter <- numIters) {
      val model = new ALS().setIterations(numIter).setBlocks(block).setAlpha(alpha)
        .setLambda(lambda)
        .setRank(rank).setSeed(seed)
        .setImplicitPrefs(implicitPrefs)
        .run(trainData)
      val validationRmse = computeRmse(model, validation, implicitPrefs)
      println("RMSE (validation) = " + validationRmse + " for the model trained with rank = "
        + rank + ", lambda = " + lambda + ", and numIter = " + numIter + ".")
      if (validationRmse < bestValidationRmse)
      {
        bestModel = Some(model)
        bestValidationRmse = validationRmse
        bestRank = rank
        bestLambda = lambda
        bestNumIter = numIter
      }
    }

    val testRmse = computeRmse(bestModel.get, test, implicitPrefs)
    println("The best model was trained with rank = " + bestRank + " and lambda = " + bestLambda
      + ", and numIter = " + bestNumIter + ", and its RMSE on the test set is " + testRmse + ".")

    val meanRating = training.union(validation).map(_.rating).mean
    val baselineRmse = math.sqrt(test.map(x => (meanRating - x.rating) * (meanRating - x.rating)).mean)
    val improvement = (baselineRmse - testRmse) / baselineRmse * 100
    println("The best model improves the baseline by " + "%1.2f".format(improvement) + "%.")

    val newUsersRatingsQuery =
      s"""
         | select * from prs.user_products_ratings_small
         | where userid in (${br_users.value.mkString(",")})
      """.stripMargin
    val newUsersRatingsDf = spark.sql(newUsersRatingsQuery)
    newUsersRatingsDf.cache()
    val newUsersRatings = newUsersRatingsDf.rdd.map(x => Rating(x.getAs[Int](0), x.getAs[Int](1), x.getAs[Double](2)))
    val completeRatings = ratings.union(newUsersRatings)

    val newModel = new ALS().setIterations(bestNumIter).setBlocks(block).setAlpha(alpha)
      .setLambda(bestLambda)
      .setRank(bestRank).setSeed(seed)
      .setImplicitPrefs(implicitPrefs)
      .run(completeRatings)

    val schema = StructType(
      List(
        StructField("product", IntegerType, true),
        StructField("user", IntegerType, true),
        StructField("rating", DoubleType, true)
      )
    )

    val len2 = br_users.value.length
    for(x <- 0 until len2) {
      val userId = br_users.value(x)
      val logPath = new Path(logFile + s"${userId}")
      val bw = new BufferedWriter(new OutputStreamWriter(fs1.create(logPath, true)))
      /*
      //val query = "select * from prs.user_products_ratings_small where userid = " + br_users.value(x)
      //val query = "select 1 as userid, movieid, rating, timestamp from prs.user_products_ratings_small where userid = 12345"
      val rec_user_df : DataFrame = newUsersRatingsDf.filter(s"userid = ${userId}")
      val userRatings = rec_user_df.rdd.map(x => Rating(x.getAs[Int](0), x.getAs[Int](1), x.getAs[Double](2)))
      val myProducts = userRatings.map(_.product).collect.toSet
      //val otherProducts2 = products.keys.filter(!myProducts.contains(_))
      //val otherProducts = spark.sparkContext.parallelize(products.keys.filter(!myProducts.contains(_)).toSeq)
      val otherProducts = products.filter(x => !myProducts.contains(x._1))
      */
      val recommendations = newModel.recommendProducts(userId, number_of_recommendations).map(x => Row(x.product, x.user, x.rating))
      val recommendationsRDD = spark.sparkContext.parallelize(recommendations)
      val recommendationsDF = spark.createDataFrame(recommendationsRDD, schema)

      val joinDF = productsDf.alias("prd").join(broadcast(recommendationsDF), productsDf.col("product_id") === recommendationsDF.col("product"), "inner")

      val finalRecommendedProducts = joinDF.select("product", "product_title", "product_category", "rating")
        .rdd
        .map(x => (x.getAs[Int](0), x.getAs[String](1), x.getAs[String](2), x.getAs[Double](3)))
        .collect()

      var i = 1
      println("Products recommended for User :: " + userId)
      bw.write("Products recommended for User :: " + userId + "\n")
      finalRecommendedProducts.foreach { r =>
        val res = "%2d".format(i) + ": " + r._1 + " ---> " + r._2 + " ---> " + r._3 + " ---> " + r._4
        println(res)
        bw.write(res + "\n")
        i += 1
      }
      bw.flush()
      bw.close()
    }
  }
}