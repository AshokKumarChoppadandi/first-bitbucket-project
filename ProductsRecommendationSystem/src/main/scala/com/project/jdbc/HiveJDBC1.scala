package com.project.jdbc

import java.sql.DriverManager

/**
  * Created by chas6003 on 17-11-2018.
  */
object HiveJDBC1 {
  def main(args: Array[String]): Unit = {
    try {
      Class.forName("org.apache.hive.jdbc.HiveDriver")
      val conn = DriverManager.getConnection("jdbc:hive2://lhrrhegapd019.enterprisenet.org:10000/prs", "", "")
      val stmt = conn.createStatement()

      val query = "select * from prs.movies where lower(title) like '%horror%' or lower(genres) like '%horror%'"
      //val query = "select * from default.movies limit 5"
      val res = stmt.executeQuery(query)
      //val tableName: String = "testHiveDriverTable1"
      //  stmt.execute("drop table if exists " + tableName)
      //stmt.execute("create table " + tableName + " (key int, value string)")
      //val sql: String = "show tables '" + tableName + "'"
      //val sql: String = "show tables"
      //System.out.println("Running: " + sql)

      //val prepStmt = conn.prepareStatement(sql)

      //val res: ResultSet = stmt.executeQuery(sql)
      //val res: ResultSet = prepStmt.getResultSet()
      while (res.next()) {
        println(res.getString(1) + " ----> " + res.getString(2) + " ----> " + res.getString(3))
      }
      /*
      while (res.next()) {
        println(res.getString(0) + " ----> " + res.getString(1) + " ----> " + res.getString(2))
      }
      */
    } catch {
      case ex: Exception => {
        ex.printStackTrace()
        System.exit(1)
      }
    }
  }
}
