package com.project.launcher

import java.net.URI
import java.util.concurrent.CountDownLatch

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.launcher.SparkLauncher
import org.apache.spark.scheduler.SparkListener

import scala.io.Source

/**
  * Created by chas6003 on 24-11-2018.
  */
object PRS_Launcher {
  def main(args: Array[String]): Unit = {
    val userId = args(0)
    val numRecommendations = args(1)
    val sparkLauncher = new SparkLauncher()
      .setSparkHome("/opt/cloudera/parcels/SPARK2/lib/spark2/")
      .setAppName("PRODUCTS_RECOMMENDATION_SYSTEM")
      .setAppResource("/home/chas6003/MovieLens/ProductsRecommendationSystem-assembly-1.0.jar")
      .setMainClass("com.project.prs.ProductsRecommendationSystem")
      .setMaster("yarn-cluster")
      .setDeployMode("cluster")
      .setVerbose(true)
      .setConf(SparkLauncher.DRIVER_MEMORY, "16G")
      .setConf(SparkLauncher.EXECUTOR_MEMORY, "16G")
      .setConf(SparkLauncher.EXECUTOR_CORES, "5")
      .setConf("spark.executor.instances", "5")
      .setConf("spark.executor.memory", "16G")
      .addSparkArg("--num-executors", "5")
      .addSparkArg("--driver-cores", "1")
      .addSparkArg("--queue", "root.cip")
      .addAppArgs(userId)
      .addAppArgs(numRecommendations)
      .launch()
      //.startApplication()

    /*
    val countDownLatch = new CountDownLatch(1);
    val sparkAppListener = new SparkListener(2000, countDownLatch);
    val appHandle = sparkLaunch.startApplication(sparkAppListener);
    val sparkAppListenerThread = new Thread(sparkAppListener);
    sparkAppListenerThread.start();
    countDownLatch.await();
    */
    sparkLauncher.waitFor()

    val hdfs = FileSystem.get(new URI("hdfs://lhrrhegapd018.enterprisenet.org:8020/"), new Configuration())
    val path2 = new Path("/user/chas6003/ProductsRecommendationSystem/Output/" + userId)
    val stream = hdfs.open(path2)

    val src = Source.fromInputStream(stream).getLines()
    src.foreach(x =>
      println(x)
    )

  }
}
