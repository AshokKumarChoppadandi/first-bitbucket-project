package com.project.load

import org.apache.spark.SparkConf
import org.apache.spark.sql.types.{IntegerType, StringType, _}
import org.apache.spark.sql.{SaveMode, SparkSession}

/**
  * Created by chas6003 on 15-11-2018.
  */
object LoadRatingsData {
  def main(args: Array[String]): Unit = {
    val inputPath1 = args(0)
    val inputPath2 = args(1)
    val hiveDb = args(2)
    val hiveTable1 = args(3)
    val hiveTable2 = args(4)

    val appName = "LOAD_RATINGS_DATA"

    val conf = new SparkConf().setAppName(appName)
    val spark = SparkSession.builder().config(conf).enableHiveSupport().getOrCreate()

    val schema1 = StructType(
      Array(
        StructField("movieId", IntegerType, nullable = true),
        StructField("title", StringType, nullable = true),
        StructField("genres", StringType, nullable = true)))
    val schema2 = StructType(
      Array(
        StructField("userId", IntegerType, nullable = true),
        StructField("movieId", IntegerType, nullable = true),
        StructField("rating", DoubleType, nullable = true),
        StructField("timestamp", LongType, nullable = true)
      )
    )
    val movies = spark.read.option("header", "true").schema(schema1).csv(inputPath1)
    movies.write.mode(SaveMode.Append).saveAsTable(s"${hiveDb}.${hiveTable1}")
    println(s"Movies Data Loaded into the table ${hiveDb}.${hiveTable1}")

    val user_ratings = spark.read.option("header", "true").schema(schema2).csv(inputPath2)
    user_ratings.write.mode(SaveMode.Append).saveAsTable(s"${hiveDb}.${hiveTable2}")
    println(s"User Ratings Data Loaded into the table ${hiveDb}.${hiveTable2}")
  }
}
