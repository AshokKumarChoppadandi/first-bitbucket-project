/**
 * Created by chas6003 on 07-05-2017.
 */
public class Factorial {

    public static int fact(int num) {
        if(num == 0) {
            return 1;
        } else {
            return num * fact(num-1);
        }
    }
    public static void main(String[] args) {
        int x = 5;
        int res = fact(x);
        System.out.println("Facorial of 5 :: " + res);
       // System.setProperty("java.io.tmpdir", "C:\\tmp");
        System.out.println(System.getProperty("java.io.tmpdir"));
    }
}

