//import junit.framework.Test;

/**
 * Created by chas6003 on 07-05-2017.
 */
public class Test1 {
    public String name;
    public int age;

    Test1(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public void display() {
        System.out.println("Name is " + name + " and Age is " + age);
    }
}
