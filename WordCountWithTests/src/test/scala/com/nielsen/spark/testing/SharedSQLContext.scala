/*
package com.nielsen.spark.testing

/**
  * Created by chas6003 on 08-05-2017.
  */

import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.{BeforeAndAfterAll, Suite}
import org.apache.spark.sql.SQLContext

trait SharedSQLContext extends BeforeAndAfterAll { self: Suite =>
  @transient private var _sc: SparkContext = _
  @transient private var _sqlContext: SQLContext = _
  def sQLContext: SQLContext = _sqlContext

  var conf = new SparkConf(false)

  override def beforeAll(): Unit = {
    _sc = new SparkContext("local[4]", "test", conf)
    _sqlContext = new SQLContext(_sc)
    super.beforeAll()
  }

  override def afterAll(): Unit = {
    LocalSparkContext.stop(_sc)
    _sqlContext = null
    _sc = null
    super.afterAll()
  }

}
*/