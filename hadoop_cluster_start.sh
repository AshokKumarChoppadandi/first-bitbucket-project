#!/bin/sh

echo "Building CentOS with Java and Scala installed...!!!"
cd CentosJavaScala
sh build.sh

echo "Building Hadoop-2.7.7 Image...!!!"
cd ../Hadoop
rm -rf slaves
echo "Adding Slaves Nodes for Hadoop Cluster...!!!"
touch slaves
echo "node2" >> slaves
echo "node3" >> slaves
sh build.sh

echo "Launching Hadoop MasterNode...!!!"
docker run -d --rm --net sparknet --ip 172.18.1.1 --hostname nodemaster --add-host node2:172.18.1.2 --add-host node3:172.18.1.3 -p 50070:50070 -p  8032:8032 -p 8020:8020 -p 50030:50030 -p 19888:19888 -p 8088:8088 --name nodemaster -it hadoop bash

echo "Launching Hadoop Slaves Nodes...!!!"
docker run -d --rm --net sparknet --ip 172.18.1.2 --hostname node2  --add-host nodemaster:172.18.1.1 --add-host node3:172.18.1.3 --link nodemaster --name node2 -it hadoop bash
docker run -d --rm --net sparknet --ip 172.18.1.3 --hostname node3  --add-host nodemaster:172.18.1.1 --add-host node2:172.18.1.2 --link nodemaster --name node3 -it hadoop bash

echo "Formatting Namenode on Hadoop Master Node...!!!"
docker exec -u hadoop -it nodemaster hdfs namenode -format

echo "Starting HDFS Daemons...!!!"
docker exec -u hadoop -it nodemaster start-dfs.sh

echo "Starting YARN Daemons...!!!"
docker exec -u hadoop -it nodemaster start-yarn.sh

echo "Starting MAPREDUCE HISTORY SERVER...!!!"
docker exec -u hadoop -it nodemaster mr-jobhistory-daemon.sh start historyserver

echo "Testing the Installation by running PI MAPREDUCE EXAMPLE"
docker exec -u hadoop -it nodemaster yarn jar hadoop-2.7.7/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.7.7.jar pi 10 10

echo "Shutting down the Hadoop Cluster...!!!"
docker stop nodemaster node2 node3
